#include <cs50.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

// make substitution ; ./substitution ABCDEFGHIJKLMNOPQRSTUVWXYZ; rm -f substitution

int printError(string);
void encrypt(string, string);

int main(int argc, char *argv[])
{
    string key = argv[1];
    if (argc == 2 && strlen(key) == 26)
    {

        for (int i = 0; i < strlen(key); i++)
        {
            if (!isalpha(key[i]))
            {
                return printError(argv[0]);
            }
            for (int j = i + 1; j < strlen(key); j++)
            {
                if (key[i] == key[j])
                {
                    return printError(argv[0]);
                }
            }
        }
        string plainText = get_string("plaintext:");

        // Encrypt
        encrypt(plainText, key);
    }
    else
    {
        return printError(argv[0]);
    }
}

void encrypt(string plainText, string key)
{
    char cipherText[strlen(plainText) + 1];
    char cipherTextArray[strlen(plainText) + 1]; // Make sure there's enough space
    // strcpy(cipherText, plainText);
    for (int i = 0; i < strlen(plainText); i++)
    {
        if (isalpha(plainText[i]))
        {

            if (isupper(plainText[i]))
            {
                // printf("%s\n",cipherText);
                cipherText[i] = (char) toupper(key[toupper(plainText[i]) - 65]);
            }

            else
            {
                // printf("%c\n",tolower(key[toupper(plainText[i]) -  65]));
                cipherText[i] = tolower(key[toupper(plainText[i]) - 65]);
            }
        }
        else
        {
            cipherText[i] = plainText[i];

            continue;
        }
    }
    cipherText[strlen(plainText)] = '\0';
    printf("ciphertext: %s\n", cipherText);
}

int printError(string programName)
{
    printf("Usage: %s key\n", programName);
    return 1;
}
