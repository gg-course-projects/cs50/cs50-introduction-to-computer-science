#include <cs50.h>
#include <ctype.h>
#import <stdio.h>
#import <string.h>

void getWinner(char player1[], char player2[]);
int sumPoints(char word[]);

int main(void)
{
    string player1 = get_string("Player 1:");
    string player2 = get_string("Player 2:");
    getWinner(player1, player2);
}

int sumPoints(char word[])
{
    char alphabet[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                       'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    char scores[] = {1, 3, 3, 2,  1, 4, 2, 4, 1, 8, 5, 1, 3,
                     1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};
    int points = 0;

    for (int i = 0; i < strlen(word); i++)
    {
        for (int j = 0; j < strlen(alphabet); j++)
        {
            if (toupper(word[i]) == alphabet[j])
            {
                points += scores[j];
                break;
            }
        }
    }
    return points;
}
void getWinner(char player1[], char player2[])
{

    int player1Score = sumPoints(player1);
    int player2Score = sumPoints(player2);

    if (player1Score > player2Score)
    {
        printf("%s\n", "Player 1 wins!");
    }
    else if (player2Score > player1Score)
    {
        printf("%s\n", "Player 2 wins!");
    }
    else
    {
        printf("%s\n", "Tie!");
    }
}
