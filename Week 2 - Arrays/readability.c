#include <cs50.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/*
index = 0.0588 * L - 0.296 * S - 15.8

 L is the average number of letters per 100 words in the text,
 S is the average number of sentences per 100 words in the text.

https://en.wikipedia.org/wiki/Coleman%E2%80%93Liau_index
L = (Letters / Words) × 100 = (639 / 119) × 100 ≈ 537
S = (Sentences / Words) × 100 = (5 / 119) × 100 ≈ 4.20
*/

int getReadingScore(char text[]);
// float mean(int arr[], int numElements);

int main(void)
{
    string text = get_string("Text: ");

    int readingScore = getReadingScore(text);
    if (readingScore < 1)
    {
        printf("Before Grade 1\n");
    }
    else if (readingScore > 15)
    {
        printf("Grade 16+\n");
    }
    else
    {
        printf("Grade %d\n", readingScore);
    }
}

int getReadingScore(char text[])
{

    int numberOfWords = 0;
    int numberOfLetters = 0;
    int numberOfSentences = 0;
    float wordAverage = 0;

    for (int i = 0; i < strlen(text); i++)
    {

        if (isspace(text[i]))
        {
            numberOfWords++;
            continue;
        }

        if (text[i] == '.' || text[i] == '!' || text[i] == '?')
        {
            numberOfSentences++;
            continue;
        }

        if (text[i] == ',' || text[i] == '\'')
        {
            continue;
        }

        numberOfLetters++;
    }
    // Count the last word
    numberOfWords++;
    // If single sentecne count it.
    if (numberOfSentences == 0)
    {
        numberOfSentences++;
    }

    float L = (numberOfLetters / (float) numberOfWords) * 100.0;
    float S = (numberOfSentences / (float) numberOfWords) * 100.0;
    float index = (0.0588 * L) - (0.296 * S) - 15.8;
    return round(index);
}
