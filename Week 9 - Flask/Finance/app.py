import os

from cs50 import SQL
from flask import Flask, flash, redirect, render_template, request, session
from flask_session import Session
from werkzeug.security import check_password_hash, generate_password_hash

from helpers import apology, login_required, lookup, usd

# Configure application
app = Flask(__name__)

# Custom filter
app.jinja_env.filters["usd"] = usd

# Configure session to use filesystem (instead of signed cookies)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///finance.db")


@app.after_request
def after_request(response):
    """Ensure responses aren't cached"""
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response


@app.route("/")
@login_required
def index():
    """Show portfolio of stocks"""
    purchases = db.execute("SELECT  symbol, SUM(shares) as shares FROM purchases WHERE user_id = ? GROUP BY symbol", session['user_id']);
    symbol_prices= {}
    for purchase in purchases:
        symbol_prices[purchase["symbol"]] = lookup(purchase["symbol"])
    return render_template('index.html', purchases = purchases, prices = symbol_prices)


@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    """Buy shares of stock"""
    if request.method == "POST":
        symbol = request.form.get('symbol')
        if symbol == '':
            return apology("Please provide a symbol")
        shares = request.form.get('shares')
        try:
            shares = int(shares)
            if shares <= 0:
                raise Exception('Shares not positive value')
        except:
            return apology("Please provide a valid number of shares.")
        symbol_data = lookup(symbol)
        if symbol_data == None:
            return apology("Quote symbol not found")
        user_data = db.execute("SELECT * FROM users WHERE id = ?", session["user_id"])
        if symbol_data['price'] * shares > float(user_data[0]['cash']):
            return apology("Could not buy shares, not enough cash")
        else:
            try:
                db.execute("INSERT INTO purchases(user_id, symbol, price, shares ,changed_at) VALUES(?,?,?,?,datetime('now'))",
                        session['user_id'], symbol,  symbol_data['price'] , shares) ;
            except Exception as e:
                return apology(f'{e}, Could not perform transaction, try again later')
            else:
                return index()
    else:
        return render_template('buy.html')


@app.route("/history")
@login_required
def history():
    """Show history of transactions"""
    purchases = db.execute("SELECT  changed_at, symbol, shares, price FROM purchases WHERE user_id = ?", session['user_id']);
    return render_template('history.html', purchases=purchases)


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in"""

    # Forget any user_id
    session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":
        # Ensure username was submitted
        if not request.form.get("username"):
            return apology("must provide username", 403)

        # Ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password", 403)

        # Query database for username
        rows = db.execute(
            "SELECT * FROM users WHERE username = ?", request.form.get("username")
        )

        # Ensure username exists and password is correct
        if len(rows) != 1 or not check_password_hash(
            rows[0]["hash"], request.form.get("password")
        ):
            return apology("invalid username and/or password", 403)

        # Remember which user has logged in
        session["user_id"] = rows[0]["id"]

        # Redirect user to home page
        return redirect("/")

    # User reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    """Get stock quote."""
    # NFLX
    if request.method == "POST":
        symbol = lookup(request.form.get('symbol'))
        if symbol == '' or symbol == None:
            return apology('Quote not found')
        return render_template('quotes.html', symbols = symbol )
    else:
        return render_template('quote.html')

@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user"""
    if request.method == "POST":
        username = request.form.get('username')
        password = request.form.get('password')
        confirmation = request.form.get('confirmation')
        if username == '' or password == '' or confirmation == '':
            return apology('Please provide all fields' )
        elif password == confirmation:
            hash = generate_password_hash(password)
            try:
                db.execute("INSERT INTO users (username, hash) VALUES (?,?)", username, hash )
            except Exception as e:
                return apology(f'{e}: Failed to regiser user, please try again' )

        else:
            return apology("Passwords don't match, please try again")
    return render_template("register.html")


@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    """Sell shares of stock"""
    if request.method == "POST":
        symbol = request.form.get('symbol')
        if symbol == '':
            return apology("Please provide a symbol")
        shares = request.form.get('shares')
        try:
            shares = int(shares)
            if shares <= 0:
                raise Exception('Shares not positive value')
        except:
            return apology("Please provide a valid number of shares.")
        total_shares = db.execute("SELECT SUM(shares) as owned_shares FROM purchases WHERE user_id = ? and symbol = ?", session["user_id"], symbol)
        if shares > total_shares[0]['owned_shares']:
            return apology("Can't sell more shares than owned.")
        symbol_data = lookup(symbol)
        if symbol_data == None:
            return apology("Quote symbol not found")
        shares_data = db.execute("SELECT id,  shares FROM purchases WHERE user_id = ? and symbol = ?;", session["user_id"], symbol)
        # for owned_shares in shares_data:
        #     if shares <= owned_shares['shares']:
        #         db.execute("UPDATE purchases SET shares = ? WHERE id = ?;", owned_shares['shares'] - shares, owned_shares['id'])
        #         return index()
        #     else:
        #         db.execute(" DELETE FROM purchases WHERE id = ?;", owned_shares['id'])
        #         shares = shares -  owned_shares['shares']
        try:
            db.execute("INSERT INTO purchases(user_id, symbol, price, shares ,changed_at) VALUES(?,?,?,?,datetime('now'))",
                    session['user_id'], symbol,  symbol_data['price'] , - shares) ;
        except Exception as e:
            return apology(f'{e}, Could not perform transaction, try again later')
        else:
            return index()
    else:
        return render_template("sell.html")

