CREATE TABLE purchases (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id integer NOT NULL, symbol TEXT NOT NULL, price NUMERIC NOT NULL, shares NUMERIC NOT NULL, changed_at TEXT NOT NULL );
CREATE INDEX userid ON purchases (user_id);
CREATE INDEX symbol ON purchases (symbol);
CREATE INDEX date ON purchases (changed_at);

("INSERT INTO purchases(user_id,symbol, price, shares ,changed_at)
VALUES(?,?,?,datetime('now'))", session['user_id'], symbol, price, shares) ;
