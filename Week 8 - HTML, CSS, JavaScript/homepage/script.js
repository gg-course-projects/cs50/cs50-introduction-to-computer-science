const HALF_MARATHON_DISTANCE = 21.0975; // kilometers
const MARATHON_DISTANCE = 42.195; // kilometers
const ULTRA_50KM_DISTANCE = 50; // kilometers
const ULTRA_100KM_DISTANCE = 100; // kilometers


document.readyState)
window.addEventListener('load', () => { console.log('hi'),onLoad();});

function calculate() {
    pace = document.getElementById('pace').value;
    speed = document.getElementById('speed').value;
    distance = document.getElementById('distance').value;
    time = document.getElementById('time').value;

    document.body.getElementById('error').value = '';

    if (pace === '' && time === '' && distance === '' && speed === ''){
        printError();
    } else if (pace !== '') {
        fillTable((Math.round((1 / pace) * 60) *10) / 10) //
    } else if(speed !== '') {
        fillTable(speed);
    } else if(time !== '' && distance !== ''){
        fillTable(distance/time);
    }

        // distance = time / pace
        // time = pace * distance
        // speed = distance / time
        // document.getElementById('pace').value = pace;

}

function printError() {
    error = document.getElementById('error');
    error.innerHTML = '<p>Please enter a value for one of the fields</p>';
    error.style.cssText = `
    color: red;
    font-size: 18px;
    font-weight: bold;
    margin-top: 5px;
    margin-bottom: 5px;
`;
}

function onLoad() {
    fillTable();
}

function fillTable(speed = 10){

    tablearea = document.getElementById('times-table');
    tablearea.innerHTML = '';
    table = document.createElement('table');

    headers = ['Distance', 'Time', 'Pace', 'Speed' ];
    rows = [50, 100,  200, 300, 400, 800, 1, 2, 3, 5, 10, "1/2 Marathon", 'Marathon', 'Ultra 50km', 'Ultra 100km'];
    for (var i = 0; i < headers.length; i++) {
        var th = document.createElement('th');
        th.appendChild( document.createTextNode(headers[i]) );
        table.appendChild(th);
    }


    for  (var r = 0; r < rows.length; r++) {
        var tr = document.createElement('tr');
        tr.appendChild( document.createElement('td') );
        if (typeof(rows[r]) === "string"){
            unit = '';
        }
        else if (rows[r] >= 50) {
            unit = ' m';
        }
        else {
            unit = ' km';
        }

        tr.cells[0].appendChild( document.createTextNode(`${rows[r]}${unit}`) );

        for (var d = 1; d < headers.length; d++) {

            tr.appendChild( document.createElement('td') );
            if (headers[d] === 'Time') {
                tr.cells[d].appendChild( document.createTextNode(`${calculateTime(rows[r], speed)}`) );
            }
            else if (headers[d] === 'Pace') {
                tr.cells[d].appendChild( document.createTextNode(`${calculatePace(rows[r], speed)}`) );
            }
            else if (headers[d] === 'Speed') {
                tr.cells[d].appendChild( document.createTextNode(`${speed} km/h`) );
            }
        }

        table.appendChild(tr);
    }

    tablearea.appendChild(table);
}

function calculateTime(distance, speed) {
    if (typeof(distance) === "string"){
        switch (distance) {
            case "1/2 Marathon":
                time = HALF_MARATHON_DISTANCE/ speed;
                break;
            case "Marathon":
                time = MARATHON_DISTANCE/ speed;
                break;
            case "Ultra 50km":
                time = ULTRA_50KM_DISTANCE / speed;
                break;
            case "Ultra 100km": {
                time = ULTRA_100KM_DISTANCE / speed;
                break;
            }
        }
        time = `${ Math.round(time * 10) / 10 } h`;
    }
    else if (distance >= 50) {
        if( time < 60 ){
            time = `${ Math.round(time * 10) / 10} s`;
        }
        else {
            time = `${ Math.round(time * 10) / 10 / 60} m`;
        }
    }
    else {
        time = distance  / speed ;
        if (time < 1) {
            time = `${  Math.round(time * 10) / 10 * 60} m`;
        } else {
            time = `${ Math.round(time * 10) / 10} h`;
        }
    }
    console.log(time);
    return time;

}

function calculatePace(distance, speed){

if (typeof distance === "string") {
    switch (distance) {
        case "1/2 Marathon":
            pace =(HALF_MARATHON_DISTANCE / speed) * 60 / HALF_MARATHON_DISTANCE;
            break;
        case "Marathon":
            pace = (MARATHON_DISTANCE / speed) * 60 / MARATHON_DISTANCE;
            break;
        case "Ultra 50km":
            pace = (ULTRA_50KM_DISTANCE / speed) * 60 / ULTRA_50KM_DISTANCE;
            break;
        case "Ultra 100km":
            pace = (ULTRA_100KM_DISTANCE / speed) * 60 / ULTRA_100KM_DISTANCE;
            break;
    }
} else {
    pace = (distance / speed) * 60 / distance;
}

// Round to 1 decimal places
pace = Math.round(pace * 10) / 10;

return `${pace} min/km`;
}

function calculateSpeed(distance, spped){


   if (typeof(distance) === "string"){
        switch (distance) {
            case "1/2 Marathon":
                speed = 21.0975 / time;
                break;
            case "Marathon":
                speed = 42.195 / time;
                break;
            case "Ultra 50km":
                speed = 50 / time;
                break;
            case "Ultra 100km": {
                speed = 100 / time;
                break;
            }
        }
    }
    else if (distance >= 50) {
        speed =  (distance / 1000) / time;

    }
    else {
        speed = distance / time;
    }
    return `${Math.round(speed)} km/h`;


}
