#include <cs50.h>
#include <stdio.h>

int main(void)
{
    int blockWith;
    do
    {
        blockWith = get_int("Please provide positive integer between 1-8, for height: ");
    }
    while (blockWith < 1 || blockWith > 8);

    char spacer = ' ';
    char block = '#';

    for (int i = 0; i < blockWith; i++)
    {
        for (int j = 0; j <= blockWith * 2; j++)
        {

            if (j == blockWith)
            {
                printf("%c%c", spacer, spacer);
            }
            else if (j == blockWith + i + 2)
            {
                break;
            }
            else if (j >= blockWith - i - 1 && j < blockWith)
            {
                printf("%c", '#');
            }
            else if (j <= blockWith + i + 1 && j > blockWith)
            {
                printf("%c", block);
            }
            else
            {
                printf("%c", spacer);
            }
        }
        printf("\n");
    }
}
