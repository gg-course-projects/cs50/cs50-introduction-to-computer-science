#include <cs50.h>
#include <stdio.h>

int getDigit();

bool isValid();

int getFirtstDigits();

string cardType();

int main(void)
{

    long creditCard = get_long("Number:");

    if (isValid(creditCard))
    {
        printf("%s", cardType(creditCard));
    }
    else
    {
        printf("%s", "INVALID\n");
    }
}

string cardType(long creditCard)
{

    long power = 1;
    int numberCount = 0;

    // Increase factor to get the first digit power
    for (long i = creditCard; i > 0; power *= 10)
    {
        i /= 10;
        numberCount++;
    }

    // American Express numbers start with 34 or 37 uses 15-digit ;
    // MasterCard numbers start with 51, 52, 53, 54, or 55  16-digit numbers,
    // Visa numbers start with 4 Visa uses 13- and 16-digit

    if (getFirtstDigits(creditCard, power, 1) == 4 && (numberCount == 13 || numberCount == 16))
    {
        return "VISA\n";
    }
    else if ((getFirtstDigits(creditCard, power, 2) >= 51 &&
              getFirtstDigits(creditCard, power, 2) <= 55) &&
             numberCount == 16)
    {
        return "MASTERCARD\n";
    }
    else if ((getFirtstDigits(creditCard, power, 2) == 34 ||
              getFirtstDigits(creditCard, power, 2) == 37) &&
             numberCount == 15)
    {
        return "AMEX\n";
    }
    else
    {
        return "INVALID\n";
    }
}

int getFirtstDigits(long creditCard, long power, int digits)
{
    // Get first two digits
    long firstTwoDigits = 0;

    for (int i = 1; i <= digits; i++)
    {
        power /= 10;
    }
    firstTwoDigits = creditCard / power;
    return firstTwoDigits;
}

int getDigit(long number, long factor)
{
    int digit = number / (factor) % 10;
    return digit;
}

bool isValid(long creditCard)
{
    long digitsLeft = creditCard;
    long factor = 1;
    short digit;
    short count = 1;
    int checksum = 0;

    while (digitsLeft > 0)
    {

        digit = getDigit(creditCard, factor);
        factor *= 10;
        digitsLeft = creditCard / factor;

        if (count % 2 == 0)
        {
            long numbersLeft = digit * 2;
            long factor1 = 1;
            short digitChecksum;
            short sumOfFactors = 0;

            if (numbersLeft > 0)
            {
                while (numbersLeft > 0)
                {
                    digitChecksum = getDigit(digit * 2, factor1);
                    factor1 *= 10;
                    numbersLeft = (digit * 2) / factor1;
                    sumOfFactors += digitChecksum;
                }
            }
            else
            {
                digitChecksum = 0;
            }
            checksum += sumOfFactors;
        }
        else
        {
            checksum += digit;
        }
        count++;
    }

    if (checksum % 10 == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
