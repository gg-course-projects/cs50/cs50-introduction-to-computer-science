import cs50


def main():
    valid_input = False

    while not valid_input:
        number = cs50.get_string("Number: ")
        try:
            int(number)
        except:  # Exception as error:
            print(f'{number} is not a valid number, please enter a number')
            # print(error)
        else:
            valid_input = True

    if validCard(number):
        #  American Express numbers start with 34 or 37;
        #  MasterCard numbers start with 51, 52, 53, 54, or 55
        #  Visa numbers start with 4.
        #  AMEX\n or MASTERCARD\n or VISA\n or INVALID\n,
        match int(number[:2:]):
            case 34 | 37:
                print("AMEX")
            case 51 | 52 | 53 | 54 | 55:
                print("MASTERCARD")
            case _:
                if int(number[:1:]) == 4:
                    print("VISA")
                else:
                    print(type(number[:1:]), number[:2:], number[:1:])
                    print("INVALID")
    else:
        print("INVALID")
    # print(luhns_number)


def validCard(number):
    odd_position = [2 * int(i) for i in number[::-1][1::2]]
    even_position = [int(i) for i in number[::-1][::2]]
    doubled_digits = []

    # print(len(number))
    if len(number) < 13:
        return False

    for i in odd_position:
        for n in str(i):
            doubled_digits.append(int(n))
    # print(odd_position)
    # print(even_position)
    # print(doubled_digits)
    # print((sum(doubled_digits) + sum(even_position)))
    # print(sum(doubled_digits))
    # print(sum(even_position))
    # print((sum(doubled_digits) + sum(even_position)) % 10)

    if (sum(doubled_digits) + sum(even_position)) % 10 == 0:
        return True
    else:
        return False


main()
