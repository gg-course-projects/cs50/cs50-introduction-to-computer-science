import csv
import sys


def main():

    profile = {}
    dna_str = []
    people = []

    # TODO: Check for command-line usage
    if len(sys.argv) < 3:
        print('Please provide database and sequence on command arguments')
    else:
        databse = sys.argv[1]
        sequence_file = sys.argv[2]
    # TODO: Read database file into a variable

    with open(databse, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            people.append(row)

    # TODO: Read DNA sequence file into a variable
        with open(sequence_file, newline='') as file:
            reader = csv.DictReader(file)
            sequence = ''.join(reader.fieldnames)

    # TODO: Find longest match of each STR in DNA sequence
    for key in people[0].keys():
        if not key == 'name':
            dna_str.append(key)
    for str in dna_str:
        profile.update({str: longest_match(sequence, str)})

    # TODO: Check database for matching profiles
    matches = {}
    for person in people:
        for key in profile:
            if not person['name'] in matches:
                matches.update({person['name']: 0})
            if int(profile[key]) == int(person[key]):
                matches[person['name']] += 1
    if int(matches[max(matches, key=matches.get)]) == int(len(people[0]) - 1):
        print(max(matches, key=matches.get))
    else:
        print("No match")

    return


def longest_match(sequence, subsequence):
    """Returns length of longest run of subsequence in sequence."""

    # Initialize variables
    longest_run = 0
    subsequence_length = len(subsequence)
    sequence_length = len(sequence)

    # Check each character in sequence for most consecutive runs of subsequence
    for i in range(sequence_length):

        # Initialize count of consecutive runs
        count = 0

        # Check for a subsequence match in a "substring" (a subset of characters) within sequence
        # If a match, move substring to next potential match in sequence
        # Continue moving substring and checking for matches until out of consecutive matches
        while True:

            # Adjust substring start and end
            start = i + count * subsequence_length
            end = start + subsequence_length

            # If there is a match in the substring
            if sequence[start:end] == subsequence:
                count += 1

            # If there is no match in the substring
            else:
                break

        # Update most consecutive matches found
        longest_run = max(longest_run, count)

    # After checking for runs at each character in seqeuence, return longest run found
    return longest_run


main()
