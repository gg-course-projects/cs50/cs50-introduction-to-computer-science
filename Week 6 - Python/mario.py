submit50 cs50/problems/2024/x/sentimental/mario/morevalid = False
while not valid:
    try:
        value = input("Height: ")
        height = int(value)
    except:
        print(f'{value} not a vailid number, please enter a whole number')
    else:
        if height > 0 and height <= 8:
            valid = True


for i in range(1, height + 1):
    print(f'{" " * (height - i)}{"#" * i}  {"#" * i}')
