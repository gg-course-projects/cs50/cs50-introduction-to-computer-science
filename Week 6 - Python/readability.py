import cs50

count_letter = 0
count_word = 0
text = cs50.get_string('Text: ')

# index = 0.0588 * L - 0.296 * S - 15.8

#  if (text[i] == '.' || text[i] == '!' || text[i] == '?')
#     {
#         numberOfSentences++;
#         continue;
#     }
# float L = (numberOfLetters / (float) numberOfWords) * 100.0;
#     float S = (numberOfSentences / (float) numberOfWords) * 100.0;
#     float index = (0.0588 * L) - (0.296 * S) - 15.8;

sentences = text.count('.') + text.count('!') + text.count('?')
words = text.split()
letters = sum([len(w.replace('?', '').replace('!', '').replace('.', '').replace(
    ',', '').replace('"', '').replace("'", '')) for w in words])

L = (letters / len(words)) * 100.0
S = (sentences / len(words)) * 100.0
index = (0.0588 * L) - (0.296 * S) - 15.8

# print(sentences)
# print({w:len(w.replace('?', '').replace('!', '').replace('.', '').replace(',', '').replace("'", '')) for w in words})
# print(words, len(words))
# print(letters)
# print(L)
# print(S)
# print((0.0588 * L))
# print(0.296 * S)
print(index)
print(round(index))
if round(index) < 1:
    print("Before Grade 1")
elif round(index) > 16:
    print("Grade 16+")
else:
    print(f'Grade {round(index)}')
