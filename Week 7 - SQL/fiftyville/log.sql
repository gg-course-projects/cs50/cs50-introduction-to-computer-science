-- get familiar with DB
.schema
-- get familiar with table
select * from crime_scene_reports ;
-- find crime report
select * from crime_scene_reports where year = 2023 and month = 07 and day = 28 and street = 'Humphrey Street';
+-----+------+-------+-----+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| id  | year | month | day |     street      |                                                                                                       description                                                                                                        |
+-----+------+-------+-----+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 295 | 2023 | 7     | 28  | Humphrey Street | Theft of the CS50 duck took place at 10:15am at the Humphrey Street bakery. Interviews were conducted today with three witnesses who were present at the time – each of their interview transcripts mentions the bakery. |
| 297 | 2023 | 7     | 28  | Humphrey Street | Littering took place at 16:36. No known witnesses.                                                                                                                                                                       |
+-----+------+-------+-----+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
-- view actual report
select * from crime_scene_reports where id = 295;
+-----+------+-------+-----+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| id  | year | month | day |     street      |                                                                                                       description                                                                                                        |
+-----+------+-------+-----+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 295 | 2023 | 7     | 28  | Humphrey Street | Theft of the CS50 duck took place at 10:15am at the Humphrey Street bakery. Interviews were conducted today with three witnesses who were present at the time – each of their interview transcripts mentions the bakery. |
+-----+------+-------+-----+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
-- read interviews
select * from interviews where transcript like '%bakery%' and month = 7;
+-----+---------+------+-------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| id  |  name   | year | month | day |                                                                                                                                                     transcript                                                                                                                                                      |
+-----+---------+------+-------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 161 | Ruth    | 2023 | 7     | 28  | Sometime within ten minutes of the theft, I saw the thief get into a car in the bakery parking lot and drive away. If you have security footage from the bakery parking lot, you might want to look for cars that left the parking lot in that time frame.                                                          |
| 162 | Eugene  | 2023 | 7     | 28  | I don't know the thief's name, but it was someone I recognized. Earlier this morning, before I arrived at Emma's bakery, I was walking by the ATM on Leggett Street and saw the thief there withdrawing some money.                                                                                                 |
| 163 | Raymond | 2023 | 7     | 28  | As the thief was leaving the bakery, they called someone who talked to them for less than a minute. In the call, I heard the thief say that they were planning to take the earliest flight out of Fiftyville tomorrow. The thief then asked the person on the other end of the phone to purchase the flight ticket. |
+-----+---------+------+-------+-----+-----------------------------------------------------------------------------------------------------------------------------------------------------
 -- check atm transactions

 select * from atm_transactions where year = 2023 and month = 07 and day = 28 and atm_location  = 'Leggett Street';
+-----+----------------+------+-------+-----+----------------+------------------+--------+
| id  | account_number | year | month | day |  atm_location  | transaction_type | amount |
+-----+----------------+------+-------+-----+----------------+------------------+--------+
| 246 | 28500762       | 2023 | 7     | 28  | Leggett Street | withdraw         | 48     |
| 264 | 28296815       | 2023 | 7     | 28  | Leggett Street | withdraw         | 20     |
| 266 | 76054385       | 2023 | 7     | 28  | Leggett Street | withdraw         | 60     |
| 267 | 49610011       | 2023 | 7     | 28  | Leggett Street | withdraw         | 50     |
| 269 | 16153065       | 2023 | 7     | 28  | Leggett Street | withdraw         | 80     |
| 288 | 25506511       | 2023 | 7     | 28  | Leggett Street | withdraw         | 20     |
| 313 | 81061156       | 2023 | 7     | 28  | Leggett Street | withdraw         | 30     |
| 336 | 26013199       | 2023 | 7     | 28  | Leggett Street | withdraw         | 35     |
+-----+----------------+------+-------+-----+----------------+------------------+--------+

-- Check accounts an poeple for transaction

select
	p.name, p.phone_number, p.passport_number, p.license_plate,
	t.amount
	from atm_transactions as t
	inner join 	bank_accounts as b on t.account_number = b.account_number
	inner join people as p on b.person_id = p.id
	where t.year = 2023 and t.month = 07 and t.day = 28 and t.atm_location  = 'Leggett Street';


+---------+----------------+-----------------+---------------+--------+
|  name   |  phone_number  | passport_number | license_plate | amount |
+---------+----------------+-----------------+---------------+--------+
| Bruce   | (367) 555-5533 | 5773159633      | 94KL13X       | 50     |
| Kaelyn  | (098) 555-1164 | 8304650265      | I449449       | 10     |
| Diana   | (770) 555-1861 | 3592750733      | 322W7JE       | 35     |
| Brooke  | (122) 555-4581 | 4408372428      | QX4YZN3       | 80     |
| Kenny   | (826) 555-1652 | 9878712108      | 30G67EN       | 20     |
| Iman    | (829) 555-5269 | 7049073643      | L93JTIZ       | 20     |
| Luca    | (389) 555-5198 | 8496433585      | 4328GD8       | 48     |
| Taylor  | (286) 555-6063 | 1988161715      | 1106N58       | 60     |
| Benista | (338) 555-6650 | 9586786673      | 8X428L0       | 30     |
+---------+----------------+-----------------+---------------+--------+

-- Check what cars from the accounts above where in the parking lot

 select * from bakery_security_logs where license_plate in ('94KL13X','I449449','322W7JE','QX4YZN3','30G67EN','L93JTIZ','4328GD8','1106N58','8X428L0') and day= 28;
+-----+------+-------+-----+------+--------+----------+---------------+
| id  | year | month | day | hour | minute | activity | license_plate |
+-----+------+-------+-----+------+--------+----------+---------------+
| 231 | 2023 | 7     | 28  | 8    | 18     | entrance | L93JTIZ       |
| 232 | 2023 | 7     | 28  | 8    | 23     | entrance | 94KL13X       |
| 237 | 2023 | 7     | 28  | 8    | 34     | entrance | 1106N58       |
| 240 | 2023 | 7     | 28  | 8    | 36     | entrance | 322W7JE       |
| 254 | 2023 | 7     | 28  | 9    | 14     | entrance | 4328GD8       |
| 261 | 2023 | 7     | 28  | 10   | 18     | exit     | 94KL13X       |
| 263 | 2023 | 7     | 28  | 10   | 19     | exit     | 4328GD8       |
| 265 | 2023 | 7     | 28  | 10   | 21     | exit     | L93JTIZ       |
| 266 | 2023 | 7     | 28  | 10   | 23     | exit     | 322W7JE       |
| 268 | 2023 | 7     | 28  | 10   | 35     | exit     | 1106N58       |
+-----+------+-------+-----+------+--------+----------+---------------+


-- view flight left on 29th morning

select a.full_name, a.city,
f.id, f.year, f.month, f.day, f.hour, f.minute
from flights as f
inner join airports as a on a.id = f.origin_airport_id
where  f.year = 2023 and f.month = 07 and f.day = 29 order by f.hour;

+-----------------------------+------------+----+------+-------+-----+------+--------+
|          full_name          |    city    | id | year | month | day | hour | minute |
+-----------------------------+------------+----+------+-------+-----+------+--------+
| Fiftyville Regional Airport | Fiftyville | 36 | 2023 | 7     | 29  | 8    | 20     |
| Fiftyville Regional Airport | Fiftyville | 43 | 2023 | 7     | 29  | 9    | 30     |
| Fiftyville Regional Airport | Fiftyville | 23 | 2023 | 7     | 29  | 12   | 15     |
| Fiftyville Regional Airport | Fiftyville | 53 | 2023 | 7     | 29  | 15   | 20     |
| Fiftyville Regional Airport | Fiftyville | 18 | 2023 | 7     | 29  | 16   | 0      |

-- people in the first flight

select *
   ...> from passengers as ps
   ...> inner join people as p on ps.passport_number = p.passport_number
   ...> where flight_id = 36;
+-----------+-----------------+------+--------+--------+----------------+-----------------+---------------+
| flight_id | passport_number | seat |   id   |  name  |  phone_number  | passport_number | license_plate |
+-----------+-----------------+------+--------+--------+----------------+-----------------+---------------+
| 36        | 7214083635      | 2A   | 953679 | Doris  | (066) 555-9701 | 7214083635      | M51FA04       |
| 36        | 1695452385      | 3B   | 398010 | Sofia  | (130) 555-0289 | 1695452385      | G412CB7       |
| 36        | 5773159633      | 4A   | 686048 | Bruce  | (367) 555-5533 | 5773159633      | 94KL13X       |
| 36        | 1540955065      | 5C   | 651714 | Edward | (328) 555-1152 | 1540955065      | 130LD9Z       |
| 36        | 8294398571      | 6C   | 560886 | Kelsey | (499) 555-9472 | 8294398571      | 0NTHK55       |
| 36        | 1988161715      | 6D   | 449774 | Taylor | (286) 555-6063 | 1988161715      | 1106N58       |
| 36        | 9878712108      | 7A   | 395717 | Kenny  | (826) 555-1652 | 9878712108      | 30G67EN       |
| 36        | 8496433585      | 7B   | 467400 | Luca   | (389) 555-5198 | 8496433585      | 4328GD8       |


- People with transacitons and in the plane

select
	p.name, p.phone_number, p.passport_number, p.license_plate,
	t.amount, t.transaction_type, t.year, t.month, t.day, t.atm_location
	from atm_transactions as t
	inner join 	bank_accounts as b on t.account_number = b.account_number
	inner join people as p on b.person_id = p.id
	where p.id in (953679,398010,686048,651714,560886,449774,395717,467400) and t.day =28 and t.transaction_type='withdraw';

+--------+----------------+-----------------+---------------+--------+------------------+------+-------+-----+----------------+
|  name  |  phone_number  | passport_number | license_plate | amount | transaction_type | year | month | day |  atm_location  |
+--------+----------------+-----------------+---------------+--------+------------------+------+-------+-----+----------------+
| Luca   | (389) 555-5198 | 8496433585      | 4328GD8       | 48     | withdraw         | 2023 | 7     | 28  | Leggett Street |
| Kenny  | (826) 555-1652 | 9878712108      | 30G67EN       | 20     | withdraw         | 2023 | 7     | 28  | Leggett Street |
| Taylor | (286) 555-6063 | 1988161715      | 1106N58       | 60     | withdraw         | 2023 | 7     | 28  | Leggett Street |
| Bruce  | (367) 555-5533 | 5773159633      | 94KL13X       | 50     | withdraw         | 2023 | 7     | 28  | Leggett Street |


-- People in fligt with trannsactions in atm and with licence plate in parking lot

sqlite> select
   ...> p.name, p.phone_number, p.passport_number, p.license_plate,
   ...> t.amount, t.year,  t.month, t.day
   ...> from atm_transactions as t
   ...> inner join bank_accounts as b on t.account_number = b.account_number
   ...> inner join people as p on b.person_id = p.id
   ...> where p.id in (953679,398010,686048,651714,560886,449774,395717,467400) and p.license_plate in ('94KL13X','I449449','322W7JE','QX4YZN3','30G67EN','L93JTIZ','4328GD8','1106N58','8X428L0') and t.day =28  and t.atm_location  = 'Leggett Street';
+--------+----------------+-----------------+---------------+--------+------+-------+-----+
|  name  |  phone_number  | passport_number | license_plate | amount | year | month | day |
+--------+----------------+-----------------+---------------+--------+------+-------+-----+
| Kenny  | (826) 555-1652 | 9878712108      | 30G67EN       | 20     | 2023 | 7     | 28  |
| Taylor | (286) 555-6063 | 1988161715      | 1106N58       | 60     | 2023 | 7     | 28  |
| Luca   | (389) 555-5198 | 8496433585      | 4328GD8       | 48     | 2023 | 7     | 28  |
| Bruce  | (367) 555-5533 | 5773159633      | 94KL13X       | 50     | 2023 | 7     | 28  |
+--------+----------------+-----------------+---------------+--------+------+-------+-----+


-- check destination city
select * from flights where  id = 36;
+----+-------------------+------------------------+------+-------+-----+------+--------+
| id | origin_airport_id | destination_airport_id | year | month | day | hour | minute |
+----+-------------------+------------------------+------+-------+-----+------+--------+
| 36 | 8                 | 4                      | 2023 | 7     | 29  | 8    | 20     |
+----+-------------------+------------------------+------+-------+-----+------+--------+

-- check airport name

sqlite> select * FROM airports where id =4;
+----+--------------+-------------------+---------------+
| id | abbreviation |     full_name     |     city      |
+----+--------------+-------------------+---------------+
| 4  | LGA          | LaGuardia Airport | New York City |----+------------------------+------+-------+-----+------+--------+
