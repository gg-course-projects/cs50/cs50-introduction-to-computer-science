// Implements a dictionary's functionality
#include <cs50.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "dictionary.h"

// Represents a node in a hash table
typedef struct node
{
    char word[LENGTH + 1];
    struct node *next;
} node;

// TODO: Choose number of buckets in hash table
const unsigned int N = 26;

// Hash table
node *table[N];

// Count of words
int numberOfWords = 0;

// Returns true if word is in dictionary, else false
bool check(const char *word)
{
    int index = hash(word);
    node *cursor = table[index];
    while (cursor != NULL)
    {
        if (strcasecmp(word, cursor->word) == 0)
        {
            return true;
        }
        cursor = cursor->next;
    }
    return false;
}

// Hashes word to a number
unsigned int hash(const char *word)
{
    // TODO: Improve this hash function
    return toupper(word[0]) - 'A';
}

// Loads dictionary into memory, returning true if successful, else false
bool load(const char *dictionary)
{
    // TODO
    FILE *file = fopen(dictionary, "r");
    if (file == NULL)
    {
        printf("Failed to open file %s\n", dictionary);
        fclose(file);
        return false;
    }
    char word[LENGTH + 1];
    int index = 0;

    while (fscanf(file, "%s", word) != EOF)
    {
        // printf("%s\n", word);

        node *n = malloc(sizeof(node));
        strcpy(n->word, word);
        n->next = NULL;
        index = hash(word);

        if (table[index] == NULL)
        {
            table[index] = n;
        }
        else if (table[index] != NULL)
        {
            n->next = table[index];
            table[index] = n;
        }
        numberOfWords++;
    }

    fclose(file);
    return true;
}

// Returns number of words in dictionary if loaded, else 0 if not yet loaded
unsigned int size(void)
{
    // TODO
    return numberOfWords;
}

// Unloads dictionary from memory, returning true if successful, else false
bool unload(void)
{
    for (int i = 0; i < N; i++)
    {
        node *next = table[i];
        node *current = table[i];

        while (next != NULL)
        {
            // set cursor to next
            current = current->next; // i+1
            free(next);
            next = current;
        }
    }
    return true;
}
