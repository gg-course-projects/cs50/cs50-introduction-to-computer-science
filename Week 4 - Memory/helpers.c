#include "helpers.h"
#include <math.h>
#include <stdio.h>

// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
    for (int h = 0; h < height; h++)
    {
        for (int w = 0; w < width; w++)
        {
            int greyValue =
                round((image[h][w].rgbtRed + image[h][w].rgbtBlue + image[h][w].rgbtGreen) / 3.0);

            image[h][w].rgbtRed = greyValue;
            image[h][w].rgbtGreen = greyValue;
            image[h][w].rgbtBlue = greyValue;
        }
    }
    return;
}

// Convert image to sepia
void sepia(int height, int width, RGBTRIPLE image[height][width])
{
    for (int h = 0; h < height; h++)
    {
        for (int w = 0; w < width; w++)
        {
            int sepiaRed = round(fminf(.393 * image[h][w].rgbtRed + .769 * image[h][w].rgbtGreen +
                                           .189 * image[h][w].rgbtBlue,
                                       255));
            int sepiaGreen = round(fminf(.349 * image[h][w].rgbtRed + .686 * image[h][w].rgbtGreen +
                                             .168 * image[h][w].rgbtBlue,
                                         255));
            int sepiaBlue = round(fminf(.272 * image[h][w].rgbtRed + .534 * image[h][w].rgbtGreen +
                                            .131 * image[h][w].rgbtBlue,
                                        255));

            image[h][w].rgbtRed = sepiaRed;
            image[h][w].rgbtGreen = sepiaGreen;
            image[h][w].rgbtBlue = sepiaBlue;
        }
    }
    return;
}

// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{
    //  if image is lareger than int??????
    for (int h = 0; h < height; h++)
    {
        for (int w = 0; w < width / 2; w++)
        {
            // printf("%i, %i\n", (w - width+1) *(-1), w);
            // invertedWith = (w - width+1) *(-1);

            int firstHalfRed = image[h][w].rgbtRed;
            int firstHalfGreen = image[h][w].rgbtGreen;
            int firstHalfBlue = image[h][w].rgbtBlue;
            int secondHalfRed = image[h][(w - width + 1) * (-1)].rgbtRed;
            int secondHalfGreen = image[h][(w - width + 1) * (-1)].rgbtGreen;
            int secondHalfBlue = image[h][(w - width + 1) * (-1)].rgbtBlue;

            // assing  image[h][w].rgbtRed to second half and image[h][(w - width+1) *(-1)].rgbtRed
            // to firsthalf

            image[h][w].rgbtRed = secondHalfRed;
            image[h][w].rgbtGreen = secondHalfGreen;
            image[h][w].rgbtBlue = secondHalfBlue;

            image[h][(w - width + 1) * (-1)].rgbtRed = firstHalfRed;
            image[h][(w - width + 1) * (-1)].rgbtGreen = firstHalfGreen;
            image[h][(w - width + 1) * (-1)].rgbtBlue = firstHalfBlue;
        }
    }
    return;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    // Create a copy of image
    RGBTRIPLE copy[height][width];
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            copy[i][j] = image[i][j];
        }
    }
    int blurRed;
    int blurGreen;
    int blurBlue;

    for (int h = 0; h < height; h++)
    {
        for (int w = 0; w < width; w++)
        {
            if (w - 1 < 0 && h - 1 < 0)
            {
                blurRed = round((copy[h][w].rgbtRed + copy[h][w + 1].rgbtRed +
                                 copy[h + 1][w].rgbtRed + copy[h + 1][w + 1].rgbtRed) /
                                4.0);

                blurGreen = round((copy[h][w].rgbtGreen + copy[h][w + 1].rgbtGreen +
                                   copy[h + 1][w].rgbtGreen + copy[h + 1][w + 1].rgbtGreen) /
                                  4.0);

                blurBlue = round((copy[h][w].rgbtBlue + copy[h][w + 1].rgbtBlue +
                                  copy[h + 1][w].rgbtBlue + copy[h + 1][w + 1].rgbtBlue) /
                                 4.0);
            }
            else if (w + 1 >= width && h - 1 < 0)
            {
                blurRed = round((copy[h + 1][w - 1].rgbtRed + copy[h + 1][w].rgbtRed +
                                 copy[h][w - 1].rgbtRed + copy[h][w].rgbtRed) /
                                4.0);

                blurGreen = round((copy[h + 1][w - 1].rgbtGreen + copy[h + 1][w].rgbtGreen +
                                   copy[h][w - 1].rgbtGreen + copy[h][w].rgbtGreen) /
                                  4.0);

                blurBlue = round((copy[h + 1][w - 1].rgbtBlue + copy[h + 1][w].rgbtBlue +
                                  copy[h][w - 1].rgbtBlue + copy[h][w].rgbtBlue) /
                                 4.0);
            }
            else if (w - 1 < 0 && h + 1 >= height)
            {
                blurRed = round((copy[h - 1][w].rgbtRed + copy[h - 1][w + 1].rgbtRed +
                                 copy[h][w].rgbtRed + copy[h][w + 1].rgbtRed) /
                                4.0);

                blurGreen = round((copy[h - 1][w].rgbtGreen + copy[h - 1][w + 1].rgbtGreen +
                                   copy[h][w].rgbtGreen + copy[h][w + 1].rgbtGreen) /
                                  4.0);

                blurBlue = round((copy[h - 1][w].rgbtBlue + copy[h - 1][w + 1].rgbtBlue +
                                  copy[h][w].rgbtBlue + copy[h][w + 1].rgbtBlue) /
                                 4.0);
            }
            else if (w + 1 >= width && h + 1 >= height)
            {
                blurRed = round((copy[h - 1][w - 1].rgbtRed + copy[h - 1][w].rgbtRed +
                                 copy[h][w - 1].rgbtRed + copy[h][w].rgbtRed) /
                                4.0);

                blurGreen = round((copy[h - 1][w - 1].rgbtGreen + copy[h - 1][w].rgbtGreen +
                                   copy[h][w - 1].rgbtGreen + copy[h][w].rgbtGreen) /
                                  4.0);

                blurBlue = round((copy[h - 1][w - 1].rgbtBlue + copy[h - 1][w].rgbtBlue +
                                  copy[h][w - 1].rgbtBlue + copy[h][w].rgbtBlue) /
                                 4.0);
            }
            else if (h - 1 < 0)
            {
                blurRed = round((copy[h][w - 1].rgbtRed + copy[h][w].rgbtRed +
                                 copy[h][w + 1].rgbtRed + copy[h + 1][w - 1].rgbtRed +
                                 copy[h + 1][w].rgbtRed + copy[h + 1][w + 1].rgbtRed) /
                                6.0);

                blurGreen = round((copy[h][w - 1].rgbtGreen + copy[h][w].rgbtGreen +
                                   copy[h][w + 1].rgbtGreen + copy[h + 1][w - 1].rgbtGreen +
                                   copy[h + 1][w].rgbtGreen + copy[h + 1][w + 1].rgbtGreen) /
                                  6.0);

                blurBlue = round((copy[h][w - 1].rgbtBlue + copy[h][w].rgbtBlue +
                                  copy[h][w + 1].rgbtBlue + copy[h + 1][w - 1].rgbtBlue +
                                  copy[h + 1][w].rgbtBlue + copy[h + 1][w + 1].rgbtBlue) /
                                 6.0);
            }
            else if (h + 1 >= height)
            {
                blurRed = round((copy[h - 1][w - 1].rgbtRed + copy[h - 1][w].rgbtRed +
                                 copy[h - 1][w + 1].rgbtRed + copy[h][w - 1].rgbtRed +
                                 copy[h][w].rgbtRed + copy[h][w + 1].rgbtRed) /
                                6.0);

                blurGreen = round((copy[h - 1][w - 1].rgbtGreen + copy[h - 1][w].rgbtGreen +
                                   copy[h - 1][w + 1].rgbtGreen + copy[h][w - 1].rgbtGreen +
                                   copy[h][w].rgbtGreen + copy[h][w + 1].rgbtGreen) /
                                  6.0);

                blurBlue = round((copy[h - 1][w - 1].rgbtBlue + copy[h - 1][w].rgbtBlue +
                                  copy[h - 1][w + 1].rgbtBlue + copy[h][w - 1].rgbtBlue +
                                  copy[h][w].rgbtBlue + copy[h][w + 1].rgbtBlue) /
                                 6.0);
            }
            else if (w - 1 < 0)
            {
                blurRed = round((copy[h - 1][w].rgbtRed + copy[h - 1][w + 1].rgbtRed +
                                 copy[h][w].rgbtRed + copy[h][w + 1].rgbtRed +
                                 copy[h + 1][w].rgbtRed + copy[h + 1][w + 1].rgbtRed) /
                                6.0);

                blurGreen = round((copy[h - 1][w].rgbtGreen + copy[h - 1][w + 1].rgbtGreen +
                                   copy[h][w].rgbtGreen + copy[h][w + 1].rgbtGreen +
                                   copy[h + 1][w].rgbtGreen + copy[h + 1][w + 1].rgbtGreen) /
                                  6.0);

                blurBlue = round((copy[h - 1][w].rgbtBlue + copy[h - 1][w + 1].rgbtBlue +
                                  copy[h][w].rgbtBlue + copy[h][w + 1].rgbtBlue +
                                  copy[h + 1][w].rgbtBlue + copy[h + 1][w + 1].rgbtBlue) /
                                 6.0);
            }
            else if (w + 1 >= width)
            {
                blurRed = round((copy[h - 1][w - 1].rgbtRed + copy[h - 1][w].rgbtRed +
                                 copy[h][w - 1].rgbtRed + copy[h][w].rgbtRed +
                                 copy[h + 1][w - 1].rgbtRed + copy[h + 1][w].rgbtRed) /
                                6.0);

                blurGreen = round((copy[h - 1][w - 1].rgbtGreen + copy[h - 1][w].rgbtGreen +
                                   copy[h][w - 1].rgbtGreen + copy[h][w].rgbtGreen +
                                   copy[h + 1][w - 1].rgbtGreen + copy[h + 1][w].rgbtGreen) /
                                  6.0);

                blurBlue = round((copy[h - 1][w - 1].rgbtBlue + copy[h - 1][w].rgbtBlue +
                                  copy[h][w - 1].rgbtBlue + copy[h][w].rgbtBlue +
                                  copy[h + 1][w - 1].rgbtBlue + copy[h + 1][w].rgbtBlue) /
                                 6.0);
            }

            else
            {
                blurRed = round((copy[h - 1][w - 1].rgbtRed + copy[h - 1][w].rgbtRed +
                                 copy[h - 1][w + 1].rgbtRed + copy[h][w - 1].rgbtRed +
                                 copy[h][w].rgbtRed + copy[h][w + 1].rgbtRed +
                                 copy[h + 1][w - 1].rgbtRed + copy[h + 1][w].rgbtRed +
                                 copy[h + 1][w + 1].rgbtRed) /
                                9.0);

                blurGreen = round((copy[h - 1][w - 1].rgbtGreen + copy[h - 1][w].rgbtGreen +
                                   copy[h - 1][w + 1].rgbtGreen + copy[h][w - 1].rgbtGreen +
                                   copy[h][w].rgbtGreen + copy[h][w + 1].rgbtGreen +
                                   copy[h + 1][w - 1].rgbtGreen + copy[h + 1][w].rgbtGreen +
                                   copy[h + 1][w + 1].rgbtGreen) /
                                  9.0);

                blurBlue = round((copy[h - 1][w - 1].rgbtBlue + copy[h - 1][w].rgbtBlue +
                                  copy[h - 1][w + 1].rgbtBlue + copy[h][w - 1].rgbtBlue +
                                  copy[h][w].rgbtBlue + copy[h][w + 1].rgbtBlue +
                                  copy[h + 1][w - 1].rgbtBlue + copy[h + 1][w].rgbtBlue +
                                  copy[h + 1][w + 1].rgbtBlue) /
                                 9.0);
            }

            image[h][w].rgbtRed = blurRed;
            image[h][w].rgbtGreen = blurGreen;
            image[h][w].rgbtBlue = blurBlue;
        }
    }
    return;
}
