// Modifies the volume of an audio file

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Number of bytes in .wav header
const int HEADER_SIZE = 44;

int main(int argc, char *argv[])
{
    // Check command-line arguments
    if (argc != 4)
    {
        printf("Usage: ./volume input.wav output.wav factor\n");
        return 1;
    }

    // Open files and determine scaling factor
    FILE *input = fopen(argv[1], "r");
    if (input == NULL)
    {
        printf("Could not open file.\n");
        return 1;
    }

    FILE *output = fopen(argv[2], "w");
    if (output == NULL)
    {
        printf("Could not open file.\n");
        return 1;
    }

    float factor = atof(argv[3]);

    // TODO: Copy header from input file to output file

    int header[HEADER_SIZE];
    //  while (fread(&header, HEADER_SIZE, 1, input))
    // {
    // printf("%li\n", ftell(input));
    fread(header, HEADER_SIZE, 1, input);
    // printf("before: %i, with addrs %p\n", *header, &header);
    fwrite(header, HEADER_SIZE, 1, output);
    // }

    // for(int i = 0; i < HEADER_SIZE; i ++){
    //     printf("%p\n", header[i]);
    // }
    // TODO: Read samples from input file and write updated data to output file
    int16_t buffer;
    // rewind(input);
    // printf("%li\n", ftell(input));
    while (fread(&buffer, sizeof(buffer), 1, input) != 0)
    {
        // printf("%li\n", ftell(input));
        // printf("before: %i, with addrs %p\n", buffer, &buffer);
        buffer *= factor;
        // printf("after: %i, with addrs %p\n\n", buffer, &buffer);
        fwrite(&buffer, sizeof(buffer), 1, output);
    }
    // printf("%li\n", ftell(input));

    // Close files
    fclose(input);
    fclose(output);
}
